# README

This is a React app that showcases various JavaScript libraries that display data.

## App Information

App Name: visual-maps

Created: Fall 2017 - Spring 2018; updated 2018, 2023

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/visual-maps)

Production: [Link](https://visual-maps.mctighecw.site)

License: MIT License

## Notes

### Frontend
* React
* Redux
* React Router
* React Router Redux
* Webpack

### JavaScript libraries
* Pixi.js
* Sigma.js
* Vis.js
* Cytoscape
* Three.js
* D3.js

## Run (with Docker)

1. Build

```
$ docker build -t visual-maps .
```

2. Run

```
$ docker run --rm -p 80:5000 --name visual-maps visual-maps
```

Last updated: 2025-02-18
