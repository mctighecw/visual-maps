const path = require('path');
const express = require('express');
const morgan = require('morgan');

const app = express();
const port = process.env.PORT || 5000;

app.set('port', port);
app.use(morgan('common'));
app.use(express.static(__dirname + '/build'));

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '/build', 'index.html'));
});

app.listen(app.get('port'), () => {
  console.log('Node app is running on port', port);
});

process.on('SIGINT', () => {
  console.info('Stopping Node server')
  process.exit(0)
})
