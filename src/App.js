import React, { Component } from 'react';
import Menu from './components/Menu';
import Routes from './Routes';
import './styles/styles.less';

class App extends Component {
  render() {
    return (
      <div>
        <Menu />
        <Routes />
      </div>
    );
  }
}

export default App;
