import React from 'react';
import { Switch, Route } from 'react-router-dom';

// App
import Home from './components/Home';

// Pixi
import DraggableIcons from './components/pixi/draggable/DraggableIcons';
import NodeMapNormal from './components/pixi/nodemaps/NodeMapNormal';
import NodeMapRandom from './components/pixi/nodemaps/NodeMapRandom';

// Sigma
import SimpleGraph from './components/sigma/SimpleGraph';
import RandomGraph from './components/sigma/RandomGraph';

// Vis
import VisGraphNormal from './components/vis/VisGraphNormal';
import VisGraphRandom from './components/vis/VisGraphRandom';

// Cytoscape
import CytoNodes from './components/cytoscape/CytoNodes';

// Three
import SpinningSphere from './components/three/SpinningSphere';
import DiscoBall from './components/three/DiscoBall';
import Plane3D from './components/three/Plane3D';

// D3
import FixedZoom from './components/d3/FixedZoom';
import ChainedTransitions from './components/d3/ChainedTransitions';
import BilevelPartition from './components/d3/BilevelPartition';

const Routes = () => (
  <div>
    <Switch>
      <Route path='/draggable-icons' component={DraggableIcons} />
      <Route path='/nodemap' component={NodeMapNormal} />
      <Route path='/nodemap-random' component={NodeMapRandom} />
      <Route path='/sigmagraph' component={SimpleGraph} />
      <Route path='/sigmagraph-random' component={RandomGraph} />
      <Route path='/visgraph' component={VisGraphNormal} />
      <Route path='/visgraph-random' component={VisGraphRandom} />
      <Route path='/cytonodes' component={CytoNodes} />
      <Route path='/spinning-sphere' component={SpinningSphere} />
      <Route path='/disco-ball' component={DiscoBall} />
      <Route path='/plane-3d' component={Plane3D} />
      <Route path='/fixed-zoom' component={FixedZoom} />
      <Route path='/chained-transitions' component={ChainedTransitions} />
      <Route path='/bilevel-partition' component={BilevelPartition} />
      <Route path='*' component={Home} />
    </Switch>
  </div>
);

export default Routes;
