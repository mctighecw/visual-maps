import React from 'react';

const Home = () => (
  <div className="home-container">
    <h1><b>Welcome</b></h1>
    <h3>This is a simple React app that showcases various JavaScript libraries.</h3>
    <h3>The theme for some of these components is computer programming languages.<br />
    There are some examples of smaller, well-organized graphs, as well as large, somewhat<br />
    chaotic graphs that have random coordinates and connections.</h3>
    <h3>Some of the components were made by other people (especially those for <i>Three</i> and <i>D3</i>).<br />
    I have included a link to the source on the bottom-right for these.</h3>
    <h3>The examples here are quite simple — they just give a small glimpse of what can<br />
    be done with such powerful tools.</h3>

    <div className="home-footer">
      <span>&copy; 2018, Christian McTighe. Coded by Hand.</span>
    </div>
  </div>
);

export default Home;
