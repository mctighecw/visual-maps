import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Dropdown from 'react-dropdown';
import '../../node_modules/react-dropdown/style.css';
import { setComponentName, setPreviousPath } from '../redux/actions';

class Menu extends Component {
  constructor (props) {
    super(props);
  }

  onPushToNewPath = (newPath, label) => {
    const pathname = this.props.routing.location.pathname;

    if (pathname === '/draggable-icons' ||
        pathname === '/spinning-sphere' ||
        pathname === '/disco-ball' ||
        pathname === '/plane-3d') {

      this.props.pushNewPath(newPath);
      window.location.reload();
    } else {
      this.props.pushNewPath(newPath);
    }
    this.props.onSetComponentName(label);
  }

  handleOnSelect = (e) => {
    const value = e.value;
    const label = e.label;
    const route = '/' + value;
    const pathname = this.props.routing.location.pathname;

    if (route !== pathname) {
      if (value === 'home') {
        this.onPushToNewPath('/', 'Select a component');
      } else {
        this.onPushToNewPath(route, label);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    const pathname = this.props.routing.location.pathname;
    const windowPathname = window.location.pathname;

    if (nextProps.routing.location.pathname !== pathname) {
      this.props.onSetPreviousPath(pathname);

      if (windowPathname === '/') {
        this.props.onSetComponentName('Select a component');
      }
    }
  }

  render() {
    const options = [
      { value: 'home', label: 'Home' },
      {
        type: 'group', name: 'pixi.js', items:
        [
          { value: 'draggable-icons', label: 'Draggable Web Dev Icons' },
          { value: 'nodemap', label: 'Pixi Node Map' },
          { value: 'nodemap-random', label: 'A Giant Random Map' }
        ]
      },
      {
        type: 'group', name: 'sigma.js', items:
        [
          { value: 'sigmagraph', label: 'Sigma Graph' },
          { value: 'sigmagraph-random', label: 'A Quite Big Random Graph' }
        ]
      },
      {
        type: 'group', name: 'vis.js', items:
        [
          { value: 'visgraph', label: 'Vis Graph' },
          { value: 'visgraph-random', label: 'A Huge Random One' }
        ]
      },
      {
        type: 'group', name: 'cytoscape.js', items:
        [
          { value: 'cytonodes', label: 'CytoNodes' }
        ]
      },
      {
      type: 'group', name: 'three.js', items:
        [
          { value: 'spinning-sphere', label: 'Spinning Sphere' },
          { value: 'disco-ball', label: 'Disco Ball' },
          { value: 'plane-3d', label: '3D Plane' }
        ]
      },
      {
        type: 'group', name: 'd3.js', items:
        [
          { value: 'fixed-zoom', label: 'Fixed Zoom' },
          { value: 'chained-transitions', label: 'Chained Transitions' },
          { value: 'bilevel-partition', label: 'Bilevel Partition' }
        ]
      }
    ];

    return (
      <div className="top-menu">
        <div className="menu-box">
          <Dropdown
            options={options}
            onChange={this.handleOnSelect}
            value={this.props.component.name}
            placeholder="Select a component"
          />
        </div>
      </div>
    )
  }
};

Menu.propTypes = {
  component: PropTypes.object.isRequired,
  routing: PropTypes.object.isRequired,
  pushNewPath: PropTypes.func.isRequired,
  onSetComponentName: PropTypes.func.isRequired,
  onSetPreviousPath: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    component: state.component,
    routing: state.routing
  }
};

const mapDispatchToProps = dispatch => {
  return {
    pushNewPath: (path) => {dispatch(push(path))},
    onSetComponentName: (name) => {dispatch(setComponentName(name))},
    onSetPreviousPath: (path) => {dispatch(setPreviousPath(path))}
  }
};

const MenuConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(Menu);

export default withRouter(MenuConnect);
