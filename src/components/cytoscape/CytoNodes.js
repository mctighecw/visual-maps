import React, { Component } from 'react';
import Nodes from './Nodes';

class CytoNodes extends Component {
  render() {
    return (
      <Nodes ref="graph" />
    )
  }
}

export default CytoNodes;
