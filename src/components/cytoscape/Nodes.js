import React, { Component } from 'react';
import cytoscape from 'cytoscape';
import { style, layout, options } from './config';
import { languages as elements } from './elements.js';

let styles = {
  display: 'block',
  height: '80vh',
  width: '90vw',
  margin: 'auto'
};

class Nodes extends Component {
  componentDidMount() {
    this.cy = cytoscape({
      container: this.refs.cyelement,
      elements: elements,
      style: style,
      layout: layout,
      options
    });
  }

  shouldComponentUpdate() {
    return false;
  }

  componentWillReceiveProps(nextProps) {
    this.cy.json(nextProps);
  }

  componentWillUnmount() {
    this.cy.destroy();
  }

  getCy() {
    return this.cy;
  }

  render() {
    return (
      <div style={styles} ref="cyelement" />
    );
  }
}

export default Nodes;
