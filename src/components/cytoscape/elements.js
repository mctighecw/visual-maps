export const languages = {
  nodes: [
    {
      "data":
      {
        "id": "n0",
        "label": "Python",
        "color": "#3572A5",
        "size": 5
      },
      position: {
        x: 150,
        y: 200
      }
    },
    {
      "data":
      {
        "id": "n1",
        "label": "JavaScript",
        "x": 3,
        "y": 1,
        "color": "#f1e05a",
        "size": 5
      },
      position: {
        x: 400,
        y: 400
      }
    },
    {
      "data":
      {
        "id": "n2",
        "label": "Ruby",
        "x": 5,
        "y": 7,
        "color": "#701516",
        "size": 3
      },
      position: {
        x: 650,
        y: 400
      }
    },
    {
      "data":
      {
        "id": "n3",
        "label": "Java",
        "x": 6,
        "y": 4,
        "color": "#b07219",
        "size": 5
      },
      position: {
        x: 450,
        y: 200
      }
    },
    {
      "data":
      {
        "id": "n4",
        "label": "C++",
        "x": 9,
        "y": 3,
        "color": "#f34b7d",
        "size": 4
      },
      position: {
        x: 50,
        y: 80
      }
    },
    {
      "data":
      {
        "id": "n5",
        "label": "Swift",
        "x": 5,
        "y": 5,
        "color": "#ffac45",
        "size": 4
      },
      position: {
        x: 60,
        y: 400
      }
    },
    {
      "data":
      {
        "id": "n6",
        "label": "Go",
        "x": 7,
        "y": 3,
        "color": "#375eab",
        "size": 2
      },
      position: {
        x: 700,
        y: 300
      }
    },
    {
      "data":
      {
        "id": "n7",
        "label": "PHP",
        "x": 4,
        "y": 8,
        "color": "#4F5D95",
        "size": 3
      },
      position: {
        x: 400,
        y: 100
      }
    },
    {
      "data":
      {
        "id": "n8",
        "label": "Scala",
        "x": 3,
        "y": 9,
        "color": "#c22d40",
        "size": 2
      },
      position: {
        x: 200,
        y: 350
      }
    }
  ]
};
