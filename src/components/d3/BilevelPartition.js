import React, { Component } from 'react';
import * as d3 from 'd3';
import { event as currentEvent } from 'd3';
import SourceBox from '../shared/SourceBox';
import * as graphData from './flare.json';

class BilevelPartition extends Component {
  componentDidMount() {
    const margin = {top: 350, right: 480, bottom: 350, left: 480};
    const radius = Math.min(margin.top, margin.right, margin.bottom, margin.left) - 10;

    const hue = d3.scale.category10();

    const luminance = d3.scale.sqrt()
      .domain([0, 1e6])
      .clamp(true)
      .range([90, 20]);

    const svg = d3.select(this.refs.main).append("svg")
      .attr("width", margin.left + margin.right)
      .attr("height", margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const partition = d3.layout.partition()
      .sort(function(a, b) { return d3.ascending(a.name, b.name); })
      .size([2 * Math.PI, radius]);

    const arc = d3.svg.arc()
      .startAngle(function(d) { return d.x; })
      .endAngle(function(d) { return d.x + d.dx ; })
      .padAngle(.01)
      .padRadius(radius / 3)
      .innerRadius(function(d) { return radius / 3 * d.depth; })
      .outerRadius(function(d) { return radius / 3 * (d.depth + 1) - 1; });

    partition
      .value(function(d) { return d.size; })
      .nodes(graphData)
      .forEach(function(d) {
        d._children = d.children;
        d.sum = d.value;
        d.key = key(d);
        d.fill = fill(d);
      });

    partition
      .children(function(d, depth) { return depth < 2 ? d._children : null; })
      .value(function(d) { return d.sum; });

    const center = svg.append("circle")
      .attr("r", radius / 3)
      .on("click", zoomOut);

    center.append("title")
      .text("zoom out");

    let path = svg.selectAll("path")
      .data(partition.nodes(graphData).slice(1))
      .enter().append("path")
      .attr("d", arc)
      .style("fill", function(d) { return d.fill; })
      .each(function(d) { this._current = updateArc(d); })
      .on("click", zoomIn);

    function zoomIn(p) {
      if (p && p.depth > 1) p = p.parent;
      if (!p.children) return;
      zoom(p, p);
    }

    function zoomOut(p) {
      if (p && p.parent) zoom(p.parent, p);
    }

    function zoom(graphData, p) {
      if (document.documentElement.__transition__) return;

      let enterArc;
      let exitArc;
      const outsideAngle = d3.scale.linear().domain([0, 2 * Math.PI]);

      function insideArc(d) {
        return p.key > d.key
          ? {depth: d.depth - 1, x: 0, dx: 0} : p.key < d.key
          ? {depth: d.depth - 1, x: 2 * Math.PI, dx: 0}
          : {depth: 0, x: 0, dx: 2 * Math.PI};
      }

      function outsideArc(d) {
        return {depth: d.depth + 1, x: outsideAngle(d.x), dx: outsideAngle(d.x + d.dx) - outsideAngle(d.x)};
      }

      center.datum(graphData);

      if (graphData === p) enterArc = outsideArc, exitArc = insideArc, outsideAngle.range([p.x, p.x + p.dx]);

      path = path.data(partition.nodes(graphData).slice(1), function(d) { return d.key; });

      if (graphData !== p) enterArc = insideArc, exitArc = outsideArc, outsideAngle.range([p.x, p.x + p.dx]);

      d3.transition().duration(currentEvent.altKey ? 7500 : 750).each(function() {
        path.exit().transition()
          .style("fill-opacity", function(d) { return d.depth === 1 + (graphData === p) ? 1 : 0; })
          .attrTween("d", function(d) { return arcTween.call(this, exitArc(d)); })
          .remove();

        path.enter().append("path")
          .style("fill-opacity", function(d) { return d.depth === 2 - (graphData === p) ? 1 : 0; })
          .style("fill", function(d) { return d.fill; })
          .on("click", zoomIn)
          .each(function(d) { this._current = enterArc(d); });

        path.transition()
          .style("fill-opacity", 1)
          .attrTween("d", function(d) { return arcTween.call(this, updateArc(d)); });
      });
    }

    function key(d) {
      let k = [], p = d;
      while (p.depth) k.push(p.name), p = p.parent;
      return k.reverse().join(".");
    }

    function fill(d) {
      let p = d;
      while (p.depth > 1) p = p.parent;
      let c = d3.lab(hue(p.name));
      c.l = luminance(d.sum);
      return c;
    }

    function arcTween(b) {
      const i = d3.interpolate(this._current, b);
      this._current = i(0);
      return function(t) {
        return arc(i(t));
      };
    }

    function updateArc(d) {
      return {depth: d.depth, x: d.x, dx: d.dx};
    }

    d3.select(self.frameElement).style("height", margin.top + margin.bottom + "px");
  }

  render() {
    return (
      <div>
        <SourceBox url="https://bl.ocks.org/mbostock/5944371" />
        <div ref="main" className="bilevel-partition" />
      </div>
    )
  }
}

export default BilevelPartition;
