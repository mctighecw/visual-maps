import React, { Component } from 'react';
import * as d3 from '../../../libs/d3.v4.min.js';
import SourceBox from '../shared/SourceBox';
import './styles.less';

class ChainedTransitions extends Component {
  componentDidMount() {
    // number of squares
    const n = 3015;

    const blue = "#3498db";
    const red = "#e74c3c";
    const grey = "#eee";

    const greyBlue = d3.interpolateRgb(grey, blue);
    const blueRed = d3.interpolateRgb(blue, red);
    const redGrey = d3.interpolateRgb(red, grey);

    d3.select(this.refs.main).selectAll("div")
      .data(d3.range(n))
      .enter().append("div")
      .transition()
        .delay(function(d, i) { return i + Math.random() * n / 4; })
        .ease(d3.easeLinear)
        .on("start", function repeat() {
          d3.active(this)
              .styleTween("background-color", function() { return greyBlue; })
            .transition()
              .delay(1000)
              .styleTween("background-color", function() { return blueRed; })
            .transition()
              .delay(1000)
              .styleTween("background-color", function() { return redGrey; })
            .transition()
              .delay(n)
              .on("start", repeat);
        });
  }

  render() {
    return (
      <div>
        <SourceBox url="https://bl.ocks.org/mbostock/70d5541b547cc222aa02" />
        <div
          ref="main"
          className="chained-transitions"
        />
      </div>
    )
  }
}

export default ChainedTransitions;
