import React, { Component } from 'react';
import * as d3 from '../../../libs/d3.v4.min.js';
import SourceBox from '../shared/SourceBox';

class FixedZoom extends Component {
  componentDidMount() {
    const canvas = d3.select(this.refs.canvas);
    const context = canvas.node().getContext("2d");
    const width = canvas.property("width");
    const height = canvas.property("height");
    const radius = 2.5;

    const points = d3.range(2000).map(phyllotaxis(10)),
      fx = points[0][0],
      fy = points[0][1];

    const zoom = d3.zoom()
      .scaleExtent([1 / 2, 96])
      .on("zoom", zoomed);

    canvas
      .call(zoom)
      .call(zoom.transform, d3.zoomIdentity);

    function zoomed() {
      const t = d3.zoomIdentity
        .translate(width / 2, height / 2)
        .scale(d3.event.transform.k)
        .translate(-fx, -fy);

      context.save();
      context.clearRect(0, 0, width, height);
      context.translate(t.x, t.y);
      context.scale(t.k, t.k);
      drawPoints();
      context.restore();
    }

    function drawPoints() {
      context.beginPath();
      points.forEach(drawPoint);
      context.fill();
    }

    function drawPoint(point) {
      context.moveTo(point[0] + radius, point[1]);
      context.arc(point[0], point[1], radius, 0, 2 * Math.PI);
    }

    function phyllotaxis(radius) {
      const theta = Math.PI * (3 - Math.sqrt(5));

      return function(i) {
        const r = radius * Math.sqrt(i), a = theta * i;
        return [
          width / 2 + r * Math.cos(a),
          height / 2 + r * Math.sin(a)
        ];
      };
    }
  }

  render() {
    return (
      <div>
        <SourceBox url="https://bl.ocks.org/mbostock/f2728977cc50761897f83323a8ea5000" />
        <canvas
          ref="canvas"
          width={window.innerWidth}
          height={window.innerHeight}
        />
      </div>
    )
  }
}

export default FixedZoom;
