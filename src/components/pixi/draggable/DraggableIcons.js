import React, { Component } from 'react';
import * as PIXI from 'pixi.js';

import android from '../../../assets/prog-icons/android.png';
import angular from '../../../assets/prog-icons/angular.png';
import css from '../../../assets/prog-icons/css.png';
import html from '../../../assets/prog-icons/html.png';
import java from '../../../assets/prog-icons/java.png';
import js from '../../../assets/prog-icons/js.png';
import node from '../../../assets/prog-icons/node.png';
import php from '../../../assets/prog-icons/php.png';
import react from '../../../assets/prog-icons/react.png';
import sass from '../../../assets/prog-icons/sass.png';

class DraggableIcons extends Component {
  render() {
    const width = window.innerWidth;
    const height = window.innerHeight;

    const app = new PIXI.Application(
      width,
      height,
      {backgroundColor: 0xfafafa}
    );

    document.body.appendChild(app.view);

    const icons = [android, angular, css, html, java, js, node, php, react, sass];

    for (let i = 0; i < icons.length; i++) {
      const x = Math.floor(Math.random() * app.renderer.width);
      const y = Math.floor(Math.random() * app.renderer.height);

      const texture = PIXI.Texture.fromImage(icons[i]);
      texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;

      let progIcon = new PIXI.Sprite(texture);
      progIcon.interactive = true;
      progIcon.buttonMode = true;
      progIcon.anchor.set(0.5);
      progIcon.scale.set(1);

      progIcon
          .on('pointerdown', function(event) {
            this.data = event.data;
            this.alpha = 0.5;
            this.dragging = true;
          })
          .on('pointerup', function() {
            this.alpha = 1;
            this.dragging = false;
            this.data = null;
          })
          .on('pointerupoutside', function() {
            this.alpha = 1;
            this.dragging = false;
            this.data = null;
          })
          .on('pointermove', function() {
            if (this.dragging) {
              const newPosition = this.data.getLocalPosition(this.parent);
              this.x = newPosition.x;
              this.y = newPosition.y;
            }
          });

      progIcon.x = x;
      progIcon.y = y;

      app.stage.addChild(progIcon);
    }

    return (
      <div />
    );
  }
};

export default DraggableIcons;
