import React, { Component } from 'react';
import * as d3 from 'd3';
import { event as currentEvent } from 'd3';
import * as PIXI from 'pixi.js';
import styles from './styles';

// data
import data from './data/organized.json';
const nodes = data.nodes;

// variables
const height = window.innerHeight;
const width = window.innerWidth;

const colorBackground = 0xffffff;
const colorGreyDark = 0x333333;
const colorGreyLight = 0xd3d3d3;

let renderer;
let stage;

class NodeMapNormal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showInfo: false,
      node: null,
      info: null
    }
  }

  animate = () => {
    renderer.render(stage);
    requestAnimationFrame(this.animate);
  }

  zoom = () => {
    let scale = currentEvent.scale;

    stage.scale.x = scale;
    stage.scale.y = scale;

    stage.position.x = currentEvent.translate[0];
    stage.position.y = currentEvent.translate[1];
  }

  componentDidMount() {
    const pixiCanvas = d3.select(this.refs.canvas);

    // setup
    renderer = new PIXI.CanvasRenderer(
      width,
      height,
      { backgroundColor: colorBackground,
        transparent: false,
        antialias: false,
        view: pixiCanvas.node()
      }
    );

    this.refs.main.appendChild(renderer.view);

    pixiCanvas.call(d3.behavior.zoom().scaleExtent([0.5, 300]).on("zoom", this.zoom))

    stage = new PIXI.Container();

    for (let i = 0; i < nodes.length; i++) {
      //node sizes
      let size = nodes[i].size;
      let diameter;

      if (size === 1) {
        diameter = 4;
      } else if (size === 2) {
        diameter = 5;
      } else if (size === 3) {
        diameter = 6;
      }

      // node circles
      let circles = new PIXI.Graphics();
      let color = nodes[i].color;
      circles.lineStyle(0.5, colorGreyDark, 1);
      circles.beginFill(color, 1);
      circles.drawCircle(0, 0, diameter);
      circles.endFill();

      circles.position.x = nodes[i].coords[0];
      circles.position.y = nodes[i].coords[1];

      stage.addChild(circles);

      // hover over node to show info
      circles.interactive = true;
      circles.hitArea = new PIXI.Circle(0, 0, 10);

      circles.mouseover = (mouseData) => {
        this.setState({
          showInfo: true,
          node: nodes[i].title,
          info: nodes[i].info,
          coords: `x: ${nodes[i].coords[0]}, y: ${nodes[i].coords[1]}`
        });
      }

      circles.mouseout = (mouseData) => {
        this.setState({
            showInfo: false,
            node: null,
            info: null,
            coords: null
          });
      }

      // node titles
      let titles = new PIXI.Text(
        nodes[i].title,
        { alpha: 0.5,
          wordWrap: true,
          fontFamily: 'Source Sans Pro',
          fontSize: 14,
          fill: colorGreyDark,
          align: 'center'
        }
      );

      titles.x = nodes[i].coords[0];
      titles.y = nodes[i].coords[1];

      stage.addChild(titles);

      // lines connecting nodes
      let lines = new PIXI.Graphics();

      for (let j = 0; j < nodes[i].links.length; j++) {
        let strength = nodes[i].links[j].strength * 0.5;
        lines.lineStyle(strength, colorGreyLight, 0.8);

        lines.moveTo(nodes[i].coords[0], nodes[i].coords[1]);
        lines.lineTo(nodes[nodes[i].links[j].id].coords[0], nodes[nodes[i].links[j].id].coords[1]);

        stage.addChild(lines);
      }
    }

    this.animate();
  }

  displayInfoArray = (info) => {
    let result = '';

    if (Array.isArray(info)) {
      info.map((item, index) => {
        if (index === info.length - 1) {
          result += item;
        } else {
          result += item + ', ';
        }
      });
    } else {
      result = info;
    }
    return result;
  }

  render() {
    const { showInfo, node, info, coords } = this.state;
    const displayInfo = this.displayInfoArray(info);

    return (
      <div>
        <div ref="main">
          <canvas ref="canvas" />
        </div>

        {showInfo ? (
          <div style={styles.infoBox}>
            <div style={styles.infoHeader}>Info</div>
            <div style={styles.infoItem}>{node}</div>
            <div style={styles.infoItem}>{displayInfo}</div>
            <div style={styles.infoItem}>Coords: {coords}</div>
          </div>
          ) : null
        }
      </div>
    );
  }
}

export default NodeMapNormal;
