// Generate node data with random coordinates based on languages.json (for Pixi.js)

// number of nodes
const length = 100;

// programming languages data
const p = require('../../../data/languages.json');

// variables
let titles = [], coords = [], links = [], sizes = [], colorsHex = [], colors0x = [], extensions = [], final = [];
let topics = [], shuffledTopics = [];

// get relevant info
for (let key in p) {
  if (p.hasOwnProperty(key)) {
    // only take entries with a color
    if (p[key].color !== undefined) {
      topics.push({ title: key, color: p[key].color, extensions: p[key].extensions });
    }
  }
}

// various functions
const shuffleTopics = () => {
  let topicsLength = topics.length;
  let numberArray = Array.apply(null, { length: topicsLength }).map(Number.call, Number);

  const shuffle = (o) => {
      for(let j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
  };

  let randomArray = shuffle(numberArray);

  for (let i = 0; i < topicsLength; i++) {
    shuffledTopics.push(topics[randomArray[i]])
  }
}

const getTopic = (i) => {
  let topic = shuffledTopics[i];
  return topic;
}

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const getSize = () => {
  let size = getRandomInt(1, 3);
  let result = size;

  return result;
}

// copy to clipboard (Mac)
const pbcopy = (data) => {
  let proc = require('child_process').spawn('pbcopy');
  proc.stdin.write(data);
  proc.stdin.end();
}

shuffleTopics();

// node title, color, extensions
for (let n = 0; n < length; ++n) {
  let topic = getTopic(n);

  titles.push(
    topic.title
  );

  colorsHex.push(
    topic.color
  );

  extensions.push(
    topic.extensions
  );
};

// convert color code
colorsHex.forEach(item => { colors0x.push(item.replace('#', '0x')); });

// coords
for (let n = 0; n < length; ++n) {
  let canvasSize;

  if (length < 50) {
    canvasSize = 1200;
  } else if (length < 100) {
    canvasSize = 1800;
  } else {
    canvasSize = 2500;
  };

  let num1 = Math.floor(Math.random() * canvasSize);
  let num2 = Math.floor(Math.random() * canvasSize);

  coords.push(
    [num1, num2]
  );
};

// links (lines) & strength
for (let n = 0; n < length; ++n) {
  let num1 = Math.floor(Math.random() * length);
  let num2 = Math.floor(Math.random() * length);
  let num3 = getRandomInt(1, 4);
  let num4 = getRandomInt(1, 4);

  links.push(
    [
      {
        "id": num1,
        "strength": num3
      },
      {
        "id": num2,
        "strength": num4
      }
    ]
  );
};

// node sizes
for (let n = 0; n < length; ++n) {
  let size = getSize();

  sizes.push(
    size
  );
};

// final array
for (let n = 0; n < length; ++n) {
  let index = n;

  final.push(
    {
      "id": index,
      "title": titles[n],
      "coords": coords[n],
      "links": links[n],
      "size": sizes[n],
      "color": colors0x[n],
      "info": extensions[n]
    }
  );
};

const output = JSON.stringify({ "nodes": final });

pbcopy(output);
console.log("Output copied to clipboard");
//console.log(output);
