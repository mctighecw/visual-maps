const styles = {
  infoBox: {
    position: 'fixed',
    display: 'block',
    bottom: 0,
    right: 0,
    width: 220,
    height: 'auto',
    backgroundColor: '#fff',
    padding: 12,
    borderRadius: 5,
    border: '1px solid #d3d3d3',
    fontFamily: 'Source Sans Pro',
    opacity: 0.9
  },
  infoHeader: {
    fontSize: 18,
    marginBottom: 10
  },
  infoItem: {
    backgroundColor: '#d3d3d3',
    color: '#666',
    fontWeight: 100,
    fontSize: 14,
    wordWrap: 'normal',
    borderRadius: 5,
    padding: 12,
    marginBottom: 8
  }
};

export default styles;
