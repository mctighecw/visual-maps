import React from 'react';
import styles from './styles';

const SourceBox = ({ url }) => (
  <div style={styles.creditsBox}>
    <a
      href={url}
      target="_blank"
      title="Source"
      style={styles.link}>
      Source
    </a>
  </div>
);

export default SourceBox;
