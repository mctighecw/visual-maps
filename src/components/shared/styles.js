const styles = {
  creditsBox: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: 'white',
    color: 'black',
    padding: 6
  },
  link: {
    color: 'black',
    fontSize: 13,
    textDecoration: 'none'
  }
};

export default styles;
