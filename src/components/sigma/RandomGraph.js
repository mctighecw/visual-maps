import React, { Component } from 'react';
import { Sigma, NodeShapes, EdgeShapes } from 'react-sigma';
import * as data from './data/random.json';
import styles from './styles';

class RandomGraph extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showInfo: false,
      info: null
    }
  }

  handleNodeHover = (e) => {
    this.setState({
      showInfo: true,
      info: {
        "label": e.data.node.label,
        "type": e.data.node.type,
        "coords": `x: ${e.data.node.x}, y: ${e.data.node.y}`
      }
    });
  }

  handleNodeUnhover = (e) => {
    this.setState({
      showInfo: false,
      info: null
    });
  }

  render() {
    const width = window.innerWidth;
    const height = window.innerHeight;
    const { showInfo, info } = this.state;

    return (
      <div>
        {showInfo ?
          <div style={styles.main}>
            <div style={styles.text}>
              <div>
                {info.label}<br />
                {info.type}<br />
                {info.coords}
              </div>
            </div>
          </div>
          : null
        }
        <Sigma
          renderer="canvas"
          graph={data}
          settings={{
            drawEdges: true,
            drawLabels: true,
            enableHovering: false,
            enableEdgeHovering: false,
            clone: false,
            verbose: true
          }}
          onOverNode={this.handleNodeHover}
          onOutNode={this.handleNodeUnhover}
          style={{height: height, width: width}}
        >
          <NodeShapes default="square" />
          <EdgeShapes default="line" />
        </Sigma>
      </div>
    )
  }
};

export default RandomGraph;
