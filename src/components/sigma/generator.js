// Generate node data with random coordinates based on languages.json (for Sigma.js)

// number of nodes
const length = 80;

// programming languages data
const p = require('../../data/languages.json');

// variables
let edges = [], nodes = [], labels = [], coords = [], sizes = [], colors = [], types = [];
let topics = [], shuffledTopics = [];

// get relevant info
for (let key in p) {
  if (p.hasOwnProperty(key)) {
    // only take entries with a color
    if (p[key].color !== undefined) {
      topics.push({ label: key, color: p[key].color, type: p[key].type });
    }
  }
}

// various functions
const shuffleTopics = () => {
  let topicsLength = topics.length;
  let numberArray = Array.apply(null, { length: topicsLength }).map(Number.call, Number);

  const shuffle = (o) => {
      for(let j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
  };

  let randomArray = shuffle(numberArray);

  for (let i = 0; i < topicsLength; i++) {
    shuffledTopics.push(topics[randomArray[i]])
  }
}

const getTopic = (i) => {
  let topic = shuffledTopics[i];
  return topic;
}

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const getSize = () => {
  let size = getRandomInt(1, 5);
  let result = size;

  return result;
}

// copy to clipboard (Mac)
const pbcopy = (data) => {
  let proc = require('child_process').spawn('pbcopy');
  proc.stdin.write(data);
  proc.stdin.end();
}

shuffleTopics();

// node label, color, type
for (let n = 0; n < length; ++n) {
  let topic = getTopic(n);

  labels.push(
    topic.label
  );

  colors.push(
    topic.color
  );

  types.push(
    topic.type
  );
};

// coords
for (let n = 0; n < length; ++n) {
  let canvasSize;

  if (length < 50) {
    canvasSize = 1200;
  } else if (length < 100) {
    canvasSize = 1800;
  } else {
    canvasSize = 2500;
  };

  let num1 = Math.floor(Math.random() * canvasSize);
  let num2 = Math.floor(Math.random() * canvasSize);

  coords.push(
    [num1, num2]
  );
};

// edges (lines)
for (let n = 0; n < length; ++n) {
  const num1 = Math.floor(Math.random() * length);
  const num2 = Math.floor(Math.random() * length);

  edges.push(
    {
      "id": `e${num1}_${num2}`,
      "source": `n${num1}`,
      "target": `n${num2}`,
      "color": "grey"
    }
  );
};

// node sizes
for (let n = 0; n < length; ++n) {
  let size = getSize();

  sizes.push(
    size
  );
};

// final node array
for (let n = 0; n < length; ++n) {
  let index = n;

  nodes.push(
    {
      "id": `n${index}`,
      "label": labels[n],
      "x": coords[n][0],
      "y": coords[n][1],
      "size": sizes[n],
      "color": colors[n],
      "type": types[n]
    }
  );
};

const output = JSON.stringify({ "edges": edges, "nodes": nodes });

pbcopy(output);
console.log("Output copied to clipboard");
//console.log(output);
