const styles = {
  main: {
    position: 'absolute',
    top: 100,
    left: 20,
    backgroundColor: 'black',
    opacity: 0.8,
    padding: 20,
    zIndex: 20
  },
  text: {
    color: 'white',
    fontSize: 14,
    lineHeight: 1.4
  }
};

export default styles;
