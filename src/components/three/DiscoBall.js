import React, { Component } from 'react';
import * as THREE from 'three';
import WindowResize from 'three-window-resize';
import SourceBox from '../shared/SourceBox';

class DiscoBall extends Component {
  componentDidMount() {
    let scene, camera, controls, sphere, sun, textureCube, loader, stars, light, light1, light2, light3, light4, light5, light6, light7, cubeCamera, renderer;

    const init = () => {
      scene = new THREE.Scene();
      scene.fog = new THREE.FogExp2(0x000000, .0007);

      const width = window.innerWidth;
      const height = window.innerHeight;

      renderer = new THREE.WebGLRenderer({
        antialias: true
      });

      renderer.setSize(width, height);
      renderer.setClearColor(0x000011, 1);
      renderer.domElement.id = "context";
      document.body.appendChild(renderer.domElement);

      camera = new THREE.PerspectiveCamera(45, width / height, .1, 10000);
      scene.add(camera);
      camera.position.set(0, 90, -290);
      camera.lookAt(scene.position);

      let windowResize = new WindowResize(renderer, camera);

      cubeCamera = new THREE.CubeCamera(1, 100000, 1024);
      scene.add(cubeCamera);

      let ambientLight = new THREE.AmbientLight(0xffffff);
      scene.add(ambientLight);

      light = new THREE.PointLight(0xFFFBE3);
      light.position.set(100, 0, -60);
      scene.add(light);

      light1 = new THREE.PointLight(0xFFFBE3);
      light1.position.set(-50, 200, 50);
      scene.add(light1);

      let intensity = 2.5;
      let distance = 100;
      let decay = 2.0;

      let c1 = 0xff0040,
        c2 = 0x0040ff,
        c3 = 0x80ff80,
        c4 = 0xffaa00,
        c5 = 0x00ffaa,
        c6 = 0xff1100;

      let dot = new THREE.SphereGeometry(0.25, 16, 8);

      light2 = new THREE.PointLight(c1, intensity, distance, decay);
      light2.add(new THREE.Mesh(dot, new THREE.MeshBasicMaterial({
        color: c1
      })));
      scene.add(light2);

      light3 = new THREE.PointLight(c2, intensity, distance, decay);
      light3.add(new THREE.Mesh(dot, new THREE.MeshBasicMaterial({
        color: c2
      })));
      scene.add(light3);

      light4 = new THREE.PointLight(c3, intensity, distance, decay);
      light4.add(new THREE.Mesh(dot, new THREE.MeshBasicMaterial({
        color: c3
      })));
      scene.add(light4);

      light5 = new THREE.PointLight(c4, intensity, distance, decay);
      light5.add(new THREE.Mesh(dot, new THREE.MeshBasicMaterial({
        color: c4
      })));
      scene.add(light5);

      light6 = new THREE.PointLight(c5, intensity, distance, decay);
      light6.add(new THREE.Mesh(dot, new THREE.MeshBasicMaterial({
        color: c5
      })));
      scene.add(light6);

      light7 = new THREE.PointLight(c6, intensity, distance, decay);
      light7.add(new THREE.Mesh(dot, new THREE.MeshBasicMaterial({
        color: c6
      })));
      scene.add(light7);
    }

    const draw = () => {
      let geo = new THREE.SphereGeometry(55, 30, 20);
      let mat = new THREE.MeshPhongMaterial({
        emissive: '#222',
        shininess: 50,
        reflectivity: 3.5,
        flatShading: true,
        specular: 'white',
        color: 'gray',
        side: THREE.DoubleSide,
        envMap: cubeCamera.renderTarget.texture,
        combine: THREE.AddOperation
      });
      sphere = new THREE.Mesh(geo, mat);
      scene.add(sphere);

      let starAmt = 15000;
      let starGeo = new THREE.SphereGeometry(1000, 100, 50);
      let starMat = {
        size: 1.0,
        opacity: 0.7
      };
      let starMesh = new THREE.PointsMaterial(starMat);

      for (let i = 0; i < starAmt; i++) {
        let starVertex = new THREE.Vector3();
        starVertex.x = Math.random() * 1000 - 500;
        starVertex.y = Math.random() * 1000 - 500;
        starVertex.z = Math.random() * 1000 - 500;
        starGeo.vertices.push(starVertex);
      }
      stars = new THREE.Points(starGeo, starMesh);
      scene.add(stars);

    }

    const animate = () => {
      requestAnimationFrame(animate);
      sphere.rotation.y += 0.005;

      let time = Date.now() * 0.0025;
      let d = 100;
      light2.position.x = Math.cos(time * 0.3) * d;
      light2.position.y = Math.cos(time * 0.1) * d;
      light2.position.z = Math.sin(time * 0.7) * d;

      light3.position.x = Math.sin(time * 0.5) * d;
      light3.position.y = Math.cos(time * 0.1) * d;
      light3.position.z = Math.sin(time * 0.5) * d;

      light4.position.x = Math.sin(time * 0.3) * d;
      light4.position.y = Math.sin(time * 0.1) * d;
      light4.position.z = Math.sin(time * 0.5) * d;

      light5.position.x = Math.cos(time * 0.3) * d;
      light5.position.y = Math.cos(time * 0.1) * d;
      light5.position.z = Math.sin(time * 0.5) * d;

      light6.position.x = Math.cos(time * 0.5) * d;
      light6.position.y = Math.sin(time * 0.3) * d;
      light6.position.z = Math.cos(time * 0.5) * d;

      light7.position.x = Math.cos(time * 0.5) * d;
      light7.position.y = Math.sin(time * 0.1) * d;
      light7.position.z = Math.cos(time * 0.5) * d;

      sphere.visible = false;
      cubeCamera.position.copy(sphere.position);
      cubeCamera.update(renderer, scene);

      sphere.visible = true;
      renderer.render(scene, camera);
    }

    init();
    draw();
    animate();
  }

  render() {
    return (
      <div>
        <SourceBox url="https://www.codeseek.co/AlisonBuki/threejs-space-disco-mEzYEB" />
        <div className="main" />
      </div>
    );
  }
}

export default DiscoBall;
