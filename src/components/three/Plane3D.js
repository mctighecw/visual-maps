import React, { Component } from 'react';
const THREE = require('three');
const OrbitControls = require('three-orbit-controls')(THREE);
const TransformControls = require('three-transform-controls')(THREE);
import WindowResize from 'three-window-resize';
import SourceBox from '../shared/SourceBox';

class Plane3D extends Component {
  componentDidMount() {
    let container;
    let camera, scene, renderer;
    let splineHelperObjects = [], splineOutline;
    let splinePointsLength = 14;
    let positions = [];
    let options;
    let geometry = new THREE.BoxGeometry( 20, 20, 20 );
    let transformControl;
    let ARC_SEGMENTS = 200;
    let splineMesh;
    let splines = {};
    let params = {
      uniform: true,
      tension: 0.5,
      centripetal: true,
      chordal: true,
      addPoint: addPoint,
      removePoint: removePoint
    };

    String.prototype.format = () => {
      let str = this;
      for ( let i = 0; i < arguments.length; i ++ ) {
        str = str.replace( '{' + i + '}', arguments[ i ] );
      }
      return str;
    };

    const init = () => {
      container = document.getElementById( 'container' );
      scene = new THREE.Scene();
      scene.background = new THREE.Color( 0xf0f0f0 );
      camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
      camera.position.set( 0, 250, 1000 );
      scene.add( camera );
      scene.add( new THREE.AmbientLight( 0xf0f0f0 ) );

      let light = new THREE.SpotLight( 0xffffff, 1.5 );
      light.position.set( 0, 1500, 200 );
      light.castShadow = true;
      light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 70, 1, 200, 2000 ) );
      light.shadow.bias = -0.000222;
      light.shadow.mapSize.width = 1024;
      light.shadow.mapSize.height = 1024;
      scene.add( light );

      let planeGeometry = new THREE.PlaneGeometry( 2000, 2000 );
      planeGeometry.rotateX( - Math.PI / 2 );

      let planeMaterial = new THREE.ShadowMaterial( { opacity: 0.2 } );

      let plane = new THREE.Mesh( planeGeometry, planeMaterial );
      plane.position.y = -200;
      plane.receiveShadow = true;
      scene.add( plane );

      let helper = new THREE.GridHelper( 2000, 100 );
      helper.position.y = - 199;
      helper.material.opacity = 0.25;
      helper.material.transparent = true;
      scene.add( helper );

      renderer = new THREE.WebGLRenderer( { antialias: true } );
      renderer.setPixelRatio( window.devicePixelRatio );
      renderer.setSize( window.innerWidth, window.innerHeight );
      renderer.shadowMap.enabled = true;
      container.appendChild( renderer.domElement );

      let windowResize = new WindowResize(renderer, camera);

      let controls = new OrbitControls( camera, renderer.domElement );
      controls.damping = 0.2;
      controls.addEventListener( 'change', render );
      controls.addEventListener( 'start', () => {
        cancelHideTransorm();
      } );
      controls.addEventListener( 'end', () => {
        delayHideTransform();
      } );

      transformControl = new TransformControls( camera, renderer.domElement );
      transformControl.addEventListener( 'change', render );
      scene.add( transformControl );

      transformControl.addEventListener( 'change', ( e ) => {
        cancelHideTransorm();
      } );
      transformControl.addEventListener( 'mouseDown', ( e ) => {
        cancelHideTransorm();
      } );
      transformControl.addEventListener( 'mouseUp', ( e ) => {
        delayHideTransform();
      } );
      transformControl.addEventListener( 'objectChange', ( e ) => {
        updateSplineOutline();
      } );

      let hiding;

      const delayHideTransform = () => {
        cancelHideTransorm();
        hideTransform();
      }

      const hideTransform = () => {
        hiding = setTimeout( () => {
          transformControl.detach( transformControl.object );
        }, 2500 )
      }

      const cancelHideTransorm = () => {
        if ( hiding ) clearTimeout( hiding );
      }

      for ( let i = 0; i < splinePointsLength; i ++ ) {
        addSplineObject( positions[ i ] );
      }

      positions = [];
      for ( let i = 0; i < splinePointsLength; i ++ ) {
        positions.push( splineHelperObjects[ i ].position );
      }

      let geometry = new THREE.Geometry();
      for ( let i = 0; i < ARC_SEGMENTS; i ++ ) {
        geometry.vertices.push( new THREE.Vector3() );
      }

      let curve = new THREE.CatmullRomCurve3( positions );
      curve.curveType = 'catmullrom';
      curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
        color: 0xff0000,
        opacity: 0.35,
        linewidth: 2
        } ) );

      curve.mesh.castShadow = true;
      splines.uniform = curve;
      curve = new THREE.CatmullRomCurve3( positions );
      curve.curveType = 'centripetal';
      curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
        color: 0x00ff00,
        opacity: 0.35,
        linewidth: 2
        } ) );

      curve.mesh.castShadow = true;
      splines.centripetal = curve;
      curve = new THREE.CatmullRomCurve3( positions );
      curve.curveType = 'chordal';
      curve.mesh = new THREE.Line( geometry.clone(), new THREE.LineBasicMaterial( {
        color: 0x0000ff,
        opacity: 0.35,
        linewidth: 2
        } ) );

      curve.mesh.castShadow = true;
      splines.chordal = curve;

      for ( let k in splines ) {
        let spline = splines[ k ];
        scene.add( spline.mesh );
      }

      load([
        new THREE.Vector3( 0, 0, 0 ),
        new THREE.Vector3( 289.76843686945404, 352.51481137238443, 376.10018915737797 ),
        new THREE.Vector3( -33.56300074753207, -51.49711742836848, -145.495472686253045 ),
        new THREE.Vector3( -81.40118730204415, 176.4306956436485, -86.958271935582161 ),
        new THREE.Vector3( -383.785318791128, 391.1365363371675, 147.869296953772746 ),
        new THREE.Vector3( 221.76843686945404, -252.51481137238443, 86.10018915737797 ),
        new THREE.Vector3( 123.56300074753207, -21.49711742836848, -124.495472686253045 ),
        new THREE.Vector3( 381.40118730204415, 66.4306956436485, 150.958271935582161 ),
        new THREE.Vector3( -189.785318791128, 48.1365363371675, 342.869296953772746 ),
        new THREE.Vector3( 151.40118730204415, 26.4306956436485, -210.958271935582161 ),
        new THREE.Vector3( 353.785318791128, 198.1365363371675, 22.869296953772746 ),
        new THREE.Vector3( -234.40118730204415, -166.4306956436485, 99.958271935582161 ),
        new THREE.Vector3( -451.785318791128, 353.1365363371675, 274.869296953772746 ),
        new THREE.Vector3( 141.40118730204415, -92.4306956436485, 116.958271935582161 )
      ]);
    }

    const addSplineObject = ( position ) => {
      let material = new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } );
      let object = new THREE.Mesh( geometry, material );

      if ( position ) {
        object.position.copy( position );
      } else {
        object.position.x = Math.random() * 1000 - 500;
        object.position.y = Math.random() * 600;
        object.position.z = Math.random() * 800 - 400;
      }
      object.castShadow = true;
      object.receiveShadow = true;
      scene.add( object );
      splineHelperObjects.push( object );
      return object;
    }

    const addPoint = () => {
      splinePointsLength ++;
      positions.push( addSplineObject().position );
      updateSplineOutline();
    }

    const removePoint = () => {
      if ( splinePointsLength <= 4 ) {
        return;
      }
      splinePointsLength --;
      positions.pop();
      scene.remove( splineHelperObjects.pop() );
      updateSplineOutline();
    }

    const updateSplineOutline = () => {
      for ( let k in splines ) {
        let spline = splines[ k ];
        splineMesh = spline.mesh;
        for ( let i = 0; i < ARC_SEGMENTS; i ++ ) {
          let p = splineMesh.geometry.vertices[ i ];
          let t = i /  ( ARC_SEGMENTS - 1 );
          spline.getPoint( t, p );
        }
        splineMesh.geometry.verticesNeedUpdate = true;
      }
    }

    const load = ( new_positions ) => {
      while ( new_positions.length > positions.length ) {
        addPoint();
      }
      while ( new_positions.length < positions.length ) {
        removePoint();
      }
      for ( let i = 0; i < positions.length; i ++ ) {
        positions[ i ].copy( new_positions[ i ] );
      }
      updateSplineOutline();
    }

    const animate = () => {
      requestAnimationFrame( animate );
      render();
      transformControl.update();
    }

    const render = () => {
      splines.uniform.mesh.visible = params.uniform;
      splines.centripetal.mesh.visible = params.centripetal;
      splines.chordal.mesh.visible = params.chordal;
      renderer.render( scene, camera );
    }

    init();
    animate();
  }

  render() {
    return (
      <div>
        <SourceBox url="https://threejs.org/examples/#webgl_geometry_spline_editor" />
        <div id="container" />
      </div>
    );
  }
}

export default Plane3D;
