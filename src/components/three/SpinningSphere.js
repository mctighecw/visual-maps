import React, { Component } from 'react';
import WindowResize from 'three-window-resize';
import * as THREE from 'three';

class SpinningSphere extends Component {
  componentDidMount() {
    let renderer = new THREE.WebGLRenderer({
      alpha: true
    });

    renderer.setClearColor (0x141414, 1);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enable = true;

    document.body.appendChild(renderer.domElement);

    let updateFcts	= [];
    let scene = new THREE.Scene();
    let camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.01, 100);
    camera.position.z = 3;

    // window resizing
    let windowResize = new WindowResize(renderer, camera);

    // lighting
    let lightAmb = new THREE.AmbientLight(0x888888);
    scene.add(lightAmb);

    let light = new THREE.DirectionalLight(0xcccccc, 1);
    light.position.set(5,5,5);
    light.castShadow = true;
    light.shadow.camera.near = 0.01;
    light.shadow.camera.far = 15;
    light.shadow.camera.fov = 45;
    light.shadow.camera.left = -1;
    light.shadow.camera.right = 1;
    light.shadow.camera.top = 1;
    light.shadow.camera.bottom = -1;
    light.shadow.bias = 0.001;
    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 1024;
    scene.add(light);

    // sphere config
    let geometry = new THREE.SphereGeometry(0.5, 32, 32);

    let material = new THREE.MeshPhongMaterial({
      wireframe: true,
      color: 0x3365ff,
      //shininess: 80,
      //flatShading: true
    });

    let mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);

    // spinning motion
    updateFcts.push(function(delta, now) {
      mesh.rotation.x -= 1/8 * delta;
      mesh.rotation.y += 1/4 * delta;
    });

    updateFcts.push(function(){
      renderer.render(scene, camera);
    });

    let lastTimeMsec = null;

    requestAnimationFrame(function animate(nowMsec) {
      requestAnimationFrame(animate);
      lastTimeMsec = lastTimeMsec || nowMsec-1000/60;
      let deltaMsec = Math.min(200, nowMsec - lastTimeMsec);
      lastTimeMsec = nowMsec;

      updateFcts.forEach(function(updateFn) {
        updateFn(deltaMsec/1000, nowMsec/1000)
      })
    });
  }

  render() {
    return (
      <div className="main" />
    );
  }
}

export default SpinningSphere;
