import React, { Component } from 'react';

import vis from 'vis/dist/vis.min.js';
const { DataSet, Network } = vis;
import 'vis/dist/vis.min.css';
import './visjs.less';

import graphOptions from './graphOptions.json';
import data from './data/random.json';

class VisGraphRandom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settingsVisible: false,
      hoverNode: false,
      hoverCoords: null,
      nodeId: null,
      nodeInfo: null
    };

    this.network;

    this.graphData = {
      nodes: new DataSet(),
      edges: new DataSet()
    };
  }

  initNetwork = () => {
    this.graphData.nodes.add(data.nodes);
    this.graphData.edges.add(data.edges);

    const options = {
      ...graphOptions,
      width: String(window.innerWidth),
      height: String(window.innerHeight)
    };

    options.configure.container = this.settingsBoard;

    this.network = new Network(this.graphElement, this.graphData, options);

    this.network.on('click', (data) => {
    });

    this.network.on('hoverNode', (data) => {
      const nodeId = this.network.getNodeAt(data.pointer.DOM);
      const nodeInfo = this.graphData.nodes.get(nodeId);

      this.setState({ hoverNode: true, hoverCoords: data.pointer.DOM, nodeInfo });
    });

    this.network.on('blurNode', (data) => {
      this.setState({ hoverNode: false, hoverCoords: null, nodeInfo: null });
    });
  }

  toggleSettingsBoard = () => {
    this.setState({ settingsVisible: !this.state.settingsVisible });
  }

  displayInfoArray = (info) => {
    let result = '';

    if (Array.isArray(info)) {
      info.map((item, index) => {
        if (index === info.length - 1) {
          result += item;
        } else {
          result += item + ', ';
        }
      });
    } else {
      result = info;
    }
    return result;
  }

  componentDidMount() {
    this.initNetwork();
  }

  render() {
    const { settingsVisible, hoverNode, hoverCoords, nodeInfo } = this.state;
    const displayInfo = nodeInfo ? this.displayInfoArray(nodeInfo.info) : null;

    return (
      <div>
        <div ref={(element) => { this.graphElement = element; }} />

        <div
          style={{
            display: settingsVisible ? 'block' : 'none',
            position: 'absolute',
            top: 110,
            right: 20,
            maxWidth: 550,
            backgroundColor: 'white',
            border: '1px solid #ccc',
            borderRadius: 5,
            opacity: 0.9,
            padding: '15px 0 15px 15px'
          }}
        >
          <h3 style={{ margin: 0 }}>Vis Settings</h3>
          <div ref={(element) => { this.settingsBoard = element; }} />
        </div>

        <div
          style={{
            position: 'absolute',
            top: 70,
            right: 20,
            fontFamily: 'Source Sans Pro',
            fontSize: 16,
            color: '#666'
          }}
          onClick={this.toggleSettingsBoard}
        >
          {settingsVisible ? "Hide Settings" : "Show Settings"}
        </div>

        {hoverNode ?
          <div
            style={{
              position: 'absolute',
              top: hoverCoords.y + 40,
              left: hoverCoords.x + 15,
              maxWidth: 200,
              backgroundColor: '#666',
              color: 'white',
              padding: 10
            }}
          >
            <b>{nodeInfo.label}</b><br />
            {displayInfo}
          </div>
          : null
        }
      </div>
    );
  }
}

export default VisGraphRandom;
