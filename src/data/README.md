# Info for `languages.json`

* JSON map of programming languages to meta data. Converted from GitHub's [Linguist YAML file](https://github.com/github/linguist/blob/master/lib/linguist/languages.yml).

* Source: https://github.com/blakeembrey/language-map

* License: MIT
