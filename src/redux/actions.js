import { SET_COMPONENT_NAME, SET_PREVIOUS_PATH } from './actionTypes.js';

export function setComponentName(name) {
  return {
    type: SET_COMPONENT_NAME,
    name
  }
};

export function setPreviousPath(path) {
  return {
    type: SET_PREVIOUS_PATH,
    path
  }
};
