import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { SET_COMPONENT_NAME, SET_PREVIOUS_PATH } from './actionTypes.js';

const initialState = {
   name: '',
   previousPath: ''
};

function component(state = initialState, action) {
  switch (action.type) {
    case SET_COMPONENT_NAME:
      return Object.assign({}, state, {
        name: action.name
      })
    case SET_PREVIOUS_PATH:
      return Object.assign({}, state, {
        previousPath: action.path
      })
    default:
      return state
  }
};

const rootReducer = combineReducers({
  component,
  routing: routerReducer
});

export default rootReducer;
