import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import { routerMiddleware } from 'react-router-redux';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from './reducers';
import browserHistory from '../history';

let items = process.env.NODE_ENV === 'development' ? (thunk, logger) : thunk;

const middlewares = compose(
  applyMiddleware(
    promise(),
    items,
    routerMiddleware(browserHistory)
  ),
  autoRehydrate()
);

const store = createStore(
  rootReducer,
  middlewares
);

persistStore(store);

export default store;
